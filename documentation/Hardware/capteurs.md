# Capteurs de température infrarouge

Plusieurs capteurs de température infrarouge (monopoint) comparée sur bases de différentes critères à identifier (prix, etc. ).

Expliquer chacun des critères et faire ensuite un tableau (et justifier un peu les + et -)

|  | Critère 1 | Critère 2 | Critère 3 |
| ------ | ------ | ------ | ------ |
| Capteur 1 | + | - | ++ |
| Capteur 2 | + | + | + |

**NB** : s'il y a beaucoup de critères et moins de capteurs, le tableau peut être transposé.


