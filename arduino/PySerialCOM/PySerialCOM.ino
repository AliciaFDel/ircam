/*!
 * @file        IRCamSerial.ino
 * @brief       this demo demonstrates how to use a serial communication to exchange data between a computer and a microcontroler with a custom protocol (ASCII)
 * @copyright   Copyright (c) ULB - LISA 2023
 * @license     The MIT License (MIT)
 * @author      [Rudy Ercek](rudy.ercek@ulb.be)
 * @version     V1.0
 * @date        2023-09-18
 * @url         
 */

/********************************************************************************************
* YOU NEED TO ADJUST THE ANALOGPIN, LEDPIN AND DIGITAL INPUTS WITH THE MICROCONTROLER GPIOs *
*********************************************************************************************/
//serial communication speed, most microcontrolers support a baud rate of 115200 bits per second
#define SERIAL_SPEED 115200
//default analog port to read (port A0), please check your microcontroler for an ADC GPIO !
#define ANALOGPIN A0
//default output for LEDPIN (use a digital)
#ifndef LEDPIN
   #define LEDPIN 13
#endif 

//DIGITAL INPUTS port for input 0,1,... ndin-1, you can put any number of digital port, 
//but don't forget to adjust the value of ndin (by default 4 pins)
const int din[] = {4,5,6,7};  
//number of digital port from 0 to ndin-1 (default 4, so 0 to 3) in din ! 
const int nbdin = 4;

/*******************************************************************************************/

//include function tools to parse a string with a number (float and integer)
#include "parsers.h"

//default serial buffer length (for parsing)
#define BUFFLG 64

//global variables for samples data (fixed number)
int sampling_period = 100; //data sampling period in ms (default 100ms --> 10Hz)
int samples=0,Samples=0,Nsamples=0;   //case sensitive variables !
unsigned long lastSample=0;//timestamp of the last "Sample"
//global variables for grabing data (fixed or infinite samples)
int Gsamples=0; 
unsigned long GNB=0; //grab (sample) number
unsigned long NB=0;
unsigned long tGstart=0; //start grab sample
//global variables for matrix data
int h=80,w=80; //matrix height (number of rows h) and width (number of columns w)
double p=0.02; //an example parameter which used as factor parameter when grabbing the matrix
bool grabmatrix=false; //grab a matrix from the analog input multiplied by the parameter p
bool Grabmatrix=false; //grab a flat matrix with indexes and all data (analog and digital) (one per line)

//read the analog input and send its value to the serial port
void readAnalogInput() {
    Serial.print(analogRead(ANALOGPIN));
}

//read all digital inputs and send their values to the serial port
void readAllDigitals() {
    for (int i=0;i<nbdin;i++) { 
      Serial.print(digitalRead(din[i])); 
      if (i<nbdin-1) Serial.print(" "); //space is the data seperator
    }
}

//read the analog input and all digital inputs
void readAll() {
  readAnalogInput();
  Serial.print(" ");
  readAllDigitals();
  Serial.println();
}

//This function should be modified with your commands, it's only an example !
bool parseCMD(char const *s) {    
    int inputInt=0; //default int variable for a command
    double inputDouble=0; //default double variable for a command
    if ( s == NULL || *s == '\0' ) {       
      return false;
    }
    switch (s[0]) { //parse the command i.e. the first char of the buffer s
    case 'a': //read the analog input
      readAnalogInput();
      Serial.println(""); //line return      
      break;
    case 'd': //read the digital value on the specified pin number listed in din e.g. d1 will read the second digital ports
      inputInt=parseInt(++s);
      if (inputInt>=nbdin) inputInt=0; //check the value enter
      Serial.println(digitalRead(din[inputInt]));            
      break;
    case 'D': //read all digital input
      readAllDigitals();
      break;
    case 'A': //read all inputs, analog first + digitals
      readAll();      
      break;    
    case 'p': //sampling period in ms ! (minimum value is set to 4ms and maximum value to 1h) e.g. p10 will set the sampling period to 10ms !
      inputInt=parseInt(++s);
      if (inputInt>=4 && inputInt<=3600000) sampling_period = inputInt;
      Serial.println(sampling_period); //print the sampling period set
      break;
    case 'f': //sampling frequency (inverse of sampling period in sec) e.g. f250 will set the frequency to 250Hz, so the period to 4ms !
      inputDouble=parseDouble(++s);
      if (inputDouble>0.00028 && inputDouble<250) sampling_period=(int)(1000.0/inputDouble);
      Serial.println(1000.0/sampling_period);
      break;
    case 's': //start sending the number of samples specified in the variables (cannot be stopped without reset !), see loop e.g. s100 will send 100 samples (one per line)
      inputInt=parseInt(++s);
      if (inputInt>=0) samples=inputInt;      
      break;
    case 'S': //start sending the number of samples specified in the variables (can be stopped by sending S0 on the serial port !), see loop e.g. S1000 will send 1000 samples (one per line) but can be stopped with S0 before the end!
      inputInt=parseInt(++s);
      if (inputInt>=0) Samples=inputInt;
      if (inputInt) Nsamples = Samples;
      lastSample=0;
      break;
    case 'G': //grab the data, see loop, similar to S but can be infinite with negative value e.g. G100 will grab 100 samples (one per line) and G-1 will grab samples till G0 is received on the serial port (to stop)
      inputInt=parseInt(++s);
      Gsamples=inputInt; 
      if (inputInt) Nsamples = Gsamples;     
      GNB=0;
      break;    
    case 'w': //specify matrix width e.g. w100 will set the matrix width to 100 (number of columns)
      inputInt=parseInt(++s);
      if (inputInt>0) w=inputInt;
      Serial.println(w);
      break;
    case 'h': //specify matrix height e.g. h90 will set the matrix height to 90 (number of rows)
      inputInt=parseInt(++s);
      if (inputInt>0) h=inputInt;
      Serial.println(h);      
      break;
    case 'c': //configure a positive double parameter p
      inputDouble=parseDouble(++s);
      if (inputDouble>0) p=inputDouble;
      Serial.println(p);      
      break;
    case 'm': //grab analog data in a matrix (one row per line and columns are seperated by spaces)
      grabmatrix=true;            
      break;
    case 'M': //grab all data in a flat file as matrix (with row and column indexes)
      Grabmatrix=true;
      break;
    case 'l': //toggle led pin (i.e. on/off)
      digitalWrite(LEDPIN, !digitalRead(LEDPIN));      
      break;    
    case 13: //ignore carriage return CR (\r)
      break;    
    case 10: //ignore line feed LF (\n)
      break;
    default: //print a message that the command is "unknown"
      Serial.print("Unknown command: ");
      Serial.println(s);      
      break;
    }
    return true;
}

//get the serial date in the buffer 
void getSerialBuffer(char *buffer) {  
  int index=0;
  int incomingByte = Serial.read();
  while (index<BUFFLG-1 && incomingByte != -1 && incomingByte != '\n' && incomingByte != '\r') {
    buffer[index++] = (char)incomingByte;
    delay(10);
    incomingByte = Serial.read();
  }
  buffer[index]=0;
}

//setup executes at the microcontroller start
void setup() {
  // put your setup code here, to run once:
  //init serial port with speed 
  Serial.begin(SERIAL_SPEED);
  //init digital ports as digital input
  for (int i=0;i<nbdin;i++) pinMode(din[i],INPUT);
  pinMode(LEDPIN,OUTPUT);
  digitalWrite(LEDPIN,HIGH); //put HIGH LEDPIN, most of the time on but sometimes off depending on the microcontroler.   
}

//main loop executes after setup (in a loop)
void loop() {  
  
  //check if somedata (i.e. command) is available on the serial port 
  if (Serial.available() > 0) { 
    char buffer[BUFFLG]; //default string buffer 
    getSerialBuffer(buffer);  //get the serial buffer    
    parseCMD(buffer);  
  } 
  
  //send samples data (analog+digital) (cannot be stopped) fast but not necessary ideal
  for (int i=0;i<samples;i++) {
     readAll();
     delay(sampling_period); //wait the sampling period but in this case readAll takes also time --> not really ideal with little sampling periods  
  }
  
  //better solution but more complex (it can be stopped during sampling by sending S0 on the serial port)
  if (Samples && (!lastSample || (millis()-lastSample>sampling_period))) {  //compute the time elapsed since the last sample was sent if a sample was already sent with lastsample
      lastSample=millis();  //millis is a function that gives the number of ms since the start of the program !
      readAll();
      Samples--; //decrement samples
      NB=Nsamples-Samples; //update number of samples acquired
  }

  //best solution using "time integration" (we also add the sample number in front of the data)
  //"Infinite" samples till stop (G0) are possible with this solution with a time limit of (2^32-1) ms from microcontroler boot!
  if (Gsamples!=0) {
    if (!GNB) { //first sample
        tGstart = millis();
        GNB++; 
        Gsamples--;
        Serial.print(GNB); Serial.print(" "); readAll();        
    } else if ((millis()-tGstart)>(GNB*sampling_period)) {  //check the total time from start and not from the previous sample
        GNB++; 
        if (Gsamples>0) Gsamples--;
        Serial.print(GNB); Serial.print(" "); readAll();        
    }
    NB=GNB; //update number of samples acquired
  }  
  
  //grab a matrix data only from the analog input multiplied by the parameter p
  //cannot be stopped !
  if (grabmatrix) {     
     for (int i=0;i<h;i++) {
       for (int j=0;j<w;j++) {
         uint16_t val=analogRead(ANALOGPIN);
         Serial.print(p*val);
         if (j<w-1) Serial.print(" "); //matrix delimiter is space         
       }
       Serial.println("");
     }
     grabmatrix=false;          
  }

  //grab a matrix data with indexes from all digital and analog input
  //cannot be stopped !
  if (Grabmatrix) {
     for (int i=0;i<h;i++) {
       for (int j=0;j<w;j++) {
         Serial.print(i);Serial.print(" ");
         Serial.print(j);Serial.print(" ");
         readAll();
       }       
     }
     Grabmatrix=false;     
  }

}
