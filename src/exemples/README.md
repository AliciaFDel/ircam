# Exemples de scripts python

Ce dossier contient 4 fonctions python de base qui vous permettent d'enregistrer et d'afficher des données acquises par un microcontrôleur programmé avec le code arduino [PySerialCOM](../../arduino/PySerialCOM).

N'hésitez pas à vous inspirer de ces codes pour créer votre logiciel final !

## Installation 

En pratique, vous n'avez besoin que d'installer [Anaconda](https://www.anaconda.com/download) qui contient de base l'ensemble des librairies i.e. `requirements.txt` nécessaires au fonctionnement de ces scripts.

Pour plus d'information, nous vous renvoyons [au site de la documentation](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/documentation/), plus spécifiquement à cette [page](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/documentation/anaconda/).

## Utilisation des scripts

Dans une [console anaconda](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/documentation/anaconda/#console-anaconda), la commande `python nom_script.py -h` permet d'avoir l'aide de ce script et les différents paramètres nécessaires pour le faire fonctionner.

## Description des scripts

### Script d'acquisition `grabmatrix.py`

Ce script permet d'acquérir une matrice de données d'un port d'entrée analogique dans un fichier en w colonnes x h lignes (méthode 0) ou dans un fichier avec, par ligne, les indices ligne et colonne de la matrice suivis de toutes les données analogique et numériques (méthode >0).

| _Arg_       | Argument            | Valeur défaut | Description |
| --------    | --------            | --------      | --------    |
| -p PORT     | --port PORT 	    | `COM3`        | Port série du microcontrôleur (COMx sous windows, la plupart /dev/ttyUSBx sous linux) |
| -f FICHIER  | --file FICHIER      | `matrix.txt`  | Fichier d'enregistrement |
| -t TIMEOUT  | --timeout TIMEOUT   | 10            | Temps d'attente en secondes des données sur le port série avant d'abandonner la lecture |
| -b BAUDRATE | --baudrate BAUDRATE | 115200        | Débit en bauds du port série |
| -c CONFIG   | --config CONFIG     | 0.02          | Facteur de conversion pour les données analogiques avec la méthode 0 |
| -W LARGEUR  | --width LARGEUR     | 100           | Largeur de la matrice c.à.d. nombre de colonnes |
| -H HAUTEUR  | --height HAUTEUR    | 80            | Hauteur de la matrice, nombre de lignes |
| -m MÉTHODE  | --method MÉTHODE    | 0             | Numéro de la méthode, 0 pour la matrice de base juste l'analogique et au-dessus pour l'index avec toutes les données |

Le fichier [`matrix.txt`](matrix.txt) donne un exemple de fichier obtenu avec la méthode 0.

**Exemple d'utilisation**  : `python grabmatrix.py -f mymatrix.txt -p COM4 -H 90 -W 110 -c 1 -m 0` permet d'obtenir une matrice 110x90 des entrées analogique dans le fichier `mymatrix.txt`.

### Script d'acquisition `grabsamples.py`

Ce script permet d'acquérir des échantillons des entrées analogique et numériques au cours du temps. Plusieurs méthodes d'acquisition sont disponibles avec ce script. Chacune de ces méthodes ajoute une première colonne _timestamp_ qui permet d'avoir le moment exact de l'acquisition de l'échantillon. 

| _Arg_       | Argument             | Valeur défaut | Description |
| --------    | --------             | --------      | --------    |
| -p PORT     | --port PORT 	     | `COM3`        | Port série du microcontrôleur (COMx sous windows, la plupart /dev/ttyUSBx sous linux) |
| -f FICHIER  | --file FICHIER       | `samples.txt` | Fichier d'enregistrement |
| -t TIMEOUT  | --timeout TIMEOUT    | 10            | Temps d'attente en secondes des données sur le port série avant d'abandonner la lecture. Veuillez choisir un délai d'attente supérieur à l'inverse de la fréquence d'échantillonnage !|
| -b BAUDRATE | --baudrate BAUDRATE  | 115200        | Débit en bauds du port série |
| -F FRÉQUENCE| --frequency FRÉQUENCE| 20			 | Fréquence d'échantillonnage des données en Hz |
| -l LONGUEUR | --length   LONGUEUR  | 600           | Longueur du fichier d'échantillons en secondes (10 min par défaut) |
| -m MÉTHODE  | --method MÉTHODE     | 0             | Numéro de la méthode, 0 pour _Grab_ `G` permettant d'avoir une grande finesse temporelle globale, 1 pour _Samples_ `S`, 2 pour demander des données avec la commande `A` à intervalle de temps fixe et 3 idem que la méthode 2 mais avec intégration du temps |

Le fichier [`samples.txt`](samples.txt) donne un exemple de fichier obtenu avec la méthode 0. Il est important de souligner que contrairement aux 3 autres méthodes, il y a une colonne supplémentaire à la deuxième place qui correspond au numéro de l'échantillon.

**Exemple d'utilisation**  : `python grabsamples.py -f mysamples.txt -p COM4 -L 3600 -F 2 -m 0` permet d'obtenir une heure d'échantillons analogique et numériques avec la méthode 0 à 2 Hz (2 échantillons par seconde), soit un total de +/-7200 échantillons dans un fichier `mysamples.txt`.

### Script d'affichage `showmatrix.py`

Ce script permet d'afficher une matrice, acquise avec la méthode 0 uniquement du script `grabmatrix.py` sous la forme d'une image avec une palette de [couleurs disponible](https://matplotlib.org/stable/tutorials/colors/colormaps.html) (jet par défaut)

| _Arg_       | Argument             | Valeur défaut | Description |
| --------    | --------             | --------      | --------    |
| -f FICHIER  | --file FICHIER       | `samples.txt` | Fichier d'enregistrement de la matrice à afficher |
| -o SORTIE   | --output SORTIE      |               | Fichier image de sortie à générer incluant son extension (.jpg, .png, ...), p/ex `matrix.png` |
| -p PALET    | --palet PALET        | `jet`         | Palette de couleur utilisée pour afficher la matrice, voir [palettes disponbles](https://matplotlib.org/stable/tutorials/colors/colormaps.html) |

**Exemple d'utilisation**  : `python showmatrix.py -f mymatrix.txt -p hot -o mymatrix.png` permet d'afficher la matrice `mymatrix.txt` dans une image avec une palette _hot_ et de la sauver dans le fichier image `mymatrix.png`.
                        
### Script d'affichage `showsamples.py`

Ce script permet d'afficher un graphique des valeurs analogiques en fonction du temps d'un fichier acquis avec la méthode 0 uniquement du script `grabsamples.py`.

| _Arg_       | Argument             | Valeur défaut | Description |
| --------    | --------             | --------      | --------    |
| -f FICHIER  | --file FICHIER       | `matrix.txt`  | Fichier d'enregistrement à afficher |
| -o SORTIE   | --output SORTIE      |               | Fichier image de sortie à générer incluant son extension (.jpg, .png, ...) et montrant le graphique, p/ex `samples.png` |

**Exemple d'utilisation**  : `python showsamples.py -f mysamples.txt -p hot -o mysamples.png` permet d'afficher le graphique des échantillons contenus dans le fichier `mysamples.txt` en fonction du temps et de le sauver, sous forme d'image, dans le fichier `mysamples.png`