#!/usr/bin/env python
import sys
import time
import serial
from argparse import ArgumentParser

#arguments parser
argparse = ArgumentParser()
argparse.add_argument('-p', '--port', type=str, help='Microcontroler serial port (COMx under windows, most of the time /dev/ttyUSBx under linux)', default="COM3")
argparse.add_argument('-f','--file', type=str, help='File output', default="matrix.txt")
argparse.add_argument('-t','--timeout', type=int, help='Time to wait for data on the serial before aborting in seconds.', default=10)
argparse.add_argument('-b','--baudrate', type=int, help='Serial port baud rate', default=115200)
argparse.add_argument('-c','--config', type=float, help='Conversion factor for analog data with method 0', default=0.02)
argparse.add_argument('-W','--width', type=int, help='Matrix width, number of columns', default=100)
argparse.add_argument('-H','--height', type=int, help='Matrix height, number of rows', default=80)
argparse.add_argument('-m','--method', type=int, help='Method number, 0 for basic matrix with one analog value and above for index with all data', default=0)
args = argparse.parse_args()

assert args.timeout > 0, "timeout should be > 0."
assert args.width > 0, "Matrix width should be >0."
assert args.height > 0, "Matrix height should be >0."
assert args.config > 0, "Conversion factor should be >0."

comPort = args.port
logFile = args.file
timeout = args.timeout

#open serial communication with the microcontroler
ser   = serial.Serial(comPort,args.baudrate, timeout=args.timeout)
#when opening serial communication, some microcontroler reset and sends some data on the serial port
time.sleep(2)    #wait 2 seconds for this data 
ser.flushInput() #flush data we don't need 
#open the file
f = open(logFile,'w')
print("Reading from: "+comPort)
print("Writing to  : "+logFile)
#set the configuration 
cmd = 'c'+str(args.config)
ser.write(bytes(cmd,'ascii'))
parameter = float(ser.readline())
print("Set the conversion factor to",parameter)
#set the matrix width
cmd = 'w'+str(args.width)
ser.write(bytes(cmd,'ascii'))
width = int(ser.readline())
print("Set the matrix width to",width)
#set the matrix height
cmd = 'w'+str(args.height)
ser.write(bytes(cmd,'ascii'))
height = int(ser.readline())
print("Set the matrix height to",height)
print("------------------------")
print("Begin grabbing matrix in 5 seconds - press CTRL+C in order to stop it abruptly !");
time.sleep(5)

if args.method == 0: # grab matrix with one value and conversion factor
    ser.write(b'm') 
else:  # grab matrix with indexes
    ser.write(b'M')         
    
tstart = time.time()    #start timestamp
ts = tstart #last sample timestamp
while time.time()-ts<timeout: #read till timeout ! (fast reading)
    try:
        byteIn = ser.read()
        if len(byteIn) != 0:
            d=byteIn.decode()
            f.write(d)
            sys.stdout.write(d)
            sys.stdout.flush()
            ts=time.time()            
    except:
        print('------------------------')
        print('Matrix is not complete because it was abruptly stopped after',time.time()-tstart,'seconds !')
        f.close()
        break
f.close()
print('Finish after',time.time()-tstart,'seconds (including timeout)')



